<?php namespace Louisjullien\Movies\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateLouisjullienMovies5 extends Migration
{
    public function up()
    {
        Schema::table('louisjullien_movies_', function($table)
        {
            $table->text('description')->default('null')->change();
            $table->integer('year')->default(null)->change();
            $table->string('slug', 191)->default('null')->change();
            $table->string('realisateur', 191)->default('null')->change();
            $table->boolean('published')->default(null)->change();
            $table->dropColumn('genre');
            $table->dropColumn('created_at');
        });
    }
    
    public function down()
    {
        Schema::table('louisjullien_movies_', function($table)
        {
            $table->text('description')->default('NULL')->change();
            $table->integer('year')->default(NULL)->change();
            $table->string('slug', 191)->default('\'null\'')->change();
            $table->string('realisateur', 191)->default('\'null\'')->change();
            $table->boolean('published')->default(NULL)->change();
            $table->string('genre', 191)->nullable()->default('\'null\'');
            $table->timestamp('created_at')->nullable()->default('NULL');
        });
    }
}

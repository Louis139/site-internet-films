<?php namespace Louisjullien\Movies\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateLouisjullienMovies3 extends Migration
{
    public function up()
    {
        Schema::table('louisjullien_movies_', function($table)
        {
            $table->string('genre')->nullable();
            $table->text('description')->default('null')->change();
            $table->integer('year')->default(null)->change();
            $table->string('slug', 191)->default('null')->change();
            $table->string('realisateur', 191)->default('null')->change();
        });
    }
    
    public function down()
    {
        Schema::table('louisjullien_movies_', function($table)
        {
            $table->dropColumn('genre');
            $table->text('description')->default('NULL')->change();
            $table->integer('year')->default(NULL)->change();
            $table->string('slug', 191)->default('NULL')->change();
            $table->string('realisateur', 191)->default('NULL')->change();
        });
    }
}
